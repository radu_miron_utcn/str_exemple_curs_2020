package str.curs1.ex1classandobject;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    public static final int NUMBER_OF_WHEELS = 4;
    private static int counter;

    private String producer;
    private String color;
    private int engineCapacityCC;

    public Car(String producer, String color, int engineCapacityCC) {
        this.producer = producer;
        this.color = color;
        this.engineCapacityCC = engineCapacityCC;
        counter++;
    }

    public void startEngine() {
        System.out.println(
                String.format("The %s %s car starts", color, producer));
    }

    public void moveForward() {
        System.out.println(
                String.format("The %s %s car moves forward", color, producer));
    }

    public void crash(Car otherCar) {
        System.out.println(
                String.format("The %s %s crashes into %s %s",
                        this.color, this.producer, otherCar.color, otherCar.producer));
    }

    public static int getCounter() {
        return counter;
    }
}
