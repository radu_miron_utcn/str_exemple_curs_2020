package str.curs1.ex1classandobject;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("BMW", "red", 3000);
        car1.startEngine();

        Car car2 = new Car("Dacia", "blue", 1600);
        car2.startEngine();

        car1.crash(car2);
        car2.crash(car1);

        Car car3 = car1; // car1 and car3 refer the exact same object!

        System.out.println(String.format("There were %d objects created ", Car.getCounter()));


        car2 = null; // from this point on the object previously referred by car2 cannot be used anymore

        for (int i = 0; i < new Random().nextInt(1000); i++) {
            new Car("", "", 1);
        }

        System.out.println(String.format("There were %d objects created ", Car.getCounter()));
    }
}
