package str.curs1.ex2inheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        BMW320 bmw320 = new BMW320("BMW", "red", 2000);
        bmw320.moveForward();
    }
}
