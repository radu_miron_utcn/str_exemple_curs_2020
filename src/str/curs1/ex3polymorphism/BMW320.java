package str.curs1.ex3polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class BMW320 extends Car {
    public BMW320(String producer, String color, int engineCapacityCC) {
        super(producer, color, engineCapacityCC);
    }

    @Override
    public void moveForward() {
        System.out.println(
                String.format("The %s BMW 320 moves forward FAST", super.getColor()));
    }
}
