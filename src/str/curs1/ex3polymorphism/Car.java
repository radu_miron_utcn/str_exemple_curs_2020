package str.curs1.ex3polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private String color;
    private int engineCapacityCC;

    public Car(String producer, String color, int engineCapacityCC) {
        this.color = color;
        this.engineCapacityCC = engineCapacityCC;
    }

    public void startEngine() {
        System.out.println(
                String.format("The %s car starts", color));
    }

    public void moveForward() {
        System.out.println(
                String.format("The %s car moves forward", color));
    }

    public void stopEngine() {
        System.out.println(
                String.format("The %s car stops", color));
    }


    public void crash(Car otherCar) {
        System.out.println(
                String.format("The %s %s crashes into %s %s",
                        this.color, otherCar.color));
    }

    public String getColor() {
        return color;
    }
}
