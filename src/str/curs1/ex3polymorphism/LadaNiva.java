package str.curs1.ex3polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class LadaNiva extends Car {

    public LadaNiva(String producer, String color, int engineCapacityCC) {
        super(producer, color, engineCapacityCC);
    }

    @Override
    public void moveForward() {
        System.out.println(
                String.format("The %s Lada Niva moves forward SLOWER", this.getColor()));
    }
}
