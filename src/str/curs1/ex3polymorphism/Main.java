package str.curs1.ex3polymorphism;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Choose: 1. BMW 320, 2. Lada Niva");

            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();

            Car car;

            switch (choice) {
                case 1:
                    car = new BMW320("BMW", "red", 2000);
                    break;
                case 2:
                    car = new LadaNiva("Lada", "blue", 1800);
                    break;
                default:
                    throw new IllegalArgumentException("Bad choice!");
            }

            car.startEngine();
            car.moveForward();
            car.stopEngine();
        }

        // this is without polymorphism
        // DRY - is broken; 3 lines of code are duplicated
//        if(choice == 1){
//            BMW320 bmw320 = new BMW320("BMW", "red", 2000);
//            bmw320.startEngine();
//            bmw320.moveForward();
//            bmw320.stopEngine();
//        } else if(choice == 2) {
//            LadaNiva ladaNiva = new LadaNiva("Lada", "blue", 1800);
//            ladaNiva.startEngine();
//            ladaNiva.moveForward();
//            ladaNiva.stopEngine();
//        } else {
//            throw new IllegalArgumentException("Bad choice!");
//        }
    }
}
