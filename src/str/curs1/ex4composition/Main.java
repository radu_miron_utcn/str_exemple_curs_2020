package str.curs1.ex4composition;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        //aggregation
        Battery battery = new Battery();
        Phone phone = new Phone(battery);
        phone.checkBattery();
        battery.getChargePercentage();

        //composition
        Phone phone1 = new Phone();
        phone1.checkBattery();

        try {
            Thread.sleep(5*60*60*1000);
        } catch (InterruptedException e) {
        }
    }
}
