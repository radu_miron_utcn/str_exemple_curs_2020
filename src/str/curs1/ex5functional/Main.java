package str.curs1.ex5functional;

import java.util.Arrays;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        List<String> names =
                Arrays.asList("andrei", "Ioan", "Marin", "Adi", "Xavier", "Dorel", "Alina", "Alex");

        // functional code
        names.stream()
                .map(n -> n.toUpperCase())  //x -> f(x); x -> x+1; n -> f(n)
                .filter(n -> n.startsWith("A"))
                .reduce((n1, n2) -> n1 + n2)
                .ifPresent(res -> System.out.println(res));

        // non-functional code
        String concatName = "";

        for (String name : names) {
            if (name.startsWith("a") || name.startsWith("A")) {
                concatName += name.toUpperCase();
            }
        }

        System.out.println(concatName);
    }
}
