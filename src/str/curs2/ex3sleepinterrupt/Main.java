package str.curs2.ex3sleepinterrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Thread slowThread = new SlowThread();
        Thread interrupter = new Interrupter(slowThread);

        slowThread.start();
        interrupter.start();
    }
}
