package str.curs2.ex5minigame;

import str.curs2.ex5minigame.engine.RockThread;
import str.curs2.ex5minigame.gui.MainCharacter;
import str.curs2.ex5minigame.gui.Rock;
import str.curs2.ex5minigame.gui.Window;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        List<Rock> rocks = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            Rock rock = new Rock(10 + (220 * i), 30);
            new RockThread(rock, i).start();
            rocks.add(rock);
        }

        MainCharacter mainCharacter = new MainCharacter();

        new Window(rocks, mainCharacter);
    }
}
