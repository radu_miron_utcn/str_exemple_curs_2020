package str.curs2.ex5minigame.engine;

import str.curs2.ex5minigame.Main;
import str.curs2.ex5minigame.gui.MainCharacter;
import str.curs2.ex5minigame.gui.Rock;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class RockThread extends Thread {
    private Rock rock;
    private MainCharacter mainCharacter;
    private int speed;

    public RockThread(Rock rock, int speed) {
        this.rock = rock;
        this.speed = speed;
    }

    @Override
    public void run() {
        while (rock.getY() < 550) { // TODO: or is collision
            try {
                Thread.sleep((speed + 1) * 100 + (speed * 30));
            } catch (InterruptedException e) {
            }

            rock.setLocation(rock.getX(), rock.getY() + 5);
            rock.repaint();
        }
    }
}
