package str.curs2.ex5minigame.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class MainCharacter extends JComponent {

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(0, 0, 40, 20);
    }
}
