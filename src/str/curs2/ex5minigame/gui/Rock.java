package str.curs2.ex5minigame.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Rock extends JComponent {

    public Rock(int x, int y) {
        this.setLocation(x, y);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 20, 20);
    }
}
