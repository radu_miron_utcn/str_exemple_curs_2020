package str.curs2.ex5minigame.gui;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Window extends JFrame {
    private List<Rock> rocks;
    private MainCharacter mainCharacter;

    public Window(List<Rock> rocks, MainCharacter mainCharacter) {
        this.rocks = rocks;
        this.mainCharacter = mainCharacter;

        setBounds(2100, 100, 500, 500);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        for (Rock rock : this.rocks) {
            rock.setBounds(rock.getX(), rock.getY(), 20, 20);
            add(rock);
        }

        mainCharacter.setBounds(230, 450, 40, 20);
        add(mainCharacter);

        this.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar() == 'a') {
                    mainCharacter.setLocation(mainCharacter.getX() - 5, mainCharacter.getY());
                } else if (e.getKeyChar() == 'd') {
                    mainCharacter.setLocation(mainCharacter.getX() + 5, mainCharacter.getY());
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        this.setVisible(true);
    }
}
