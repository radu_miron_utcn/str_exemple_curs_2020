package str.curs3.ex1correct;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Object lock;

    public MyThread(Object lock) {
        this.lock = lock;
    }

    public void run() {
        this.activity(1);

        synchronized (lock) {
            this.activity(2);
        }

        this.activity(3);
    }

    private void activity(int n) {
        System.out.println(Thread.currentThread().getName() + " - is executing activity " + n);

        try {
            Thread.sleep(n == 2 ? 5000 : 1000);
        } catch (InterruptedException e) {
        }
    }
}