package str.curs3.ex3;

import java.io.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileAccess {
    private File f = new File("myFile.txt");
    private PrintWriter pw = new PrintWriter(new FileWriter(f, true), true);
    private BufferedReader br = new BufferedReader(new FileReader(f));

    public FileAccess() throws IOException {
//        f.createNewFile();
    }

    public synchronized void writeLine(String line) {
        line.chars().forEach(c -> pw.println((char) c));
    }

    public synchronized String readLine() throws IOException {
        if (br.ready()) {
            return br.readLine();
        } else {
            return null;
        }
    }
}