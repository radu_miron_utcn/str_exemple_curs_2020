package str.curs3.ex3;

import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        FileAccess fileAccess = new FileAccess();

        new WriterThread(fileAccess).start();
        new WriterThread(fileAccess).start();
    }
}
