package str.curs3.ex3;

/**
 * @author Radu Miron
 * @version 1
 */
public class WriterThread extends Thread {
    private FileAccess fileAccess;

    public WriterThread(FileAccess fileAccess) {
        this.fileAccess = fileAccess;
    }

    @Override
    public void run() {
        String line = Thread.currentThread().getName().endsWith("0")
                ? "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                : "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
        fileAccess.writeLine(line);
    }
}
