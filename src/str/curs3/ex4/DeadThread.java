package str.curs3.ex4;

/**
 * @author Radu Miron
 * @version 1
 */
public class DeadThread extends Thread {
    private Object l1;
    private Object l2;

    public DeadThread(Object l1, Object l2) {
        this.l1 = l1;
        this.l2 = l2;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is starting");

        synchronized (l1) {
            System.out.println(Thread.currentThread().getName() + " entered the first sync block");

            synchronized (l2) {
                System.out.println(Thread.currentThread().getName() + " entered the second sync block");
            }
        }

        System.out.println(Thread.currentThread().getName() + " is finishing");

    }
}
