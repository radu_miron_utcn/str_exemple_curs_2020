package str.curs3.ex4;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object a = new Object();
        Object b = new Object();

        new DeadThread(a, b).start();
        new DeadThread(b, a).start();
    }
}
