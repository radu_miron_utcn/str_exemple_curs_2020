package str.curs3.ex5timer;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock = new Object();
        Win win = new Win(lock);

        new TimerThread(lock, win).start();
    }
}
