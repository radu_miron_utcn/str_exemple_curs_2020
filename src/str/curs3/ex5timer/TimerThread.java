package str.curs3.ex5timer;

/**
 * @author Radu Miron
 * @version 1
 */
public class TimerThread extends Thread {
    private Object lock;
    private Win win;

    public TimerThread(Object lock, Win win) {
        this.lock = lock;
        this.win = win;
    }

    @Override
    public void run() {
        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
            }

            long t1 = System.currentTimeMillis();

            try {
                lock.wait();
            } catch (InterruptedException e) {
            }

            long t2 = System.currentTimeMillis();

            win.updateTime(t2 - t1);
        }
    }
}
