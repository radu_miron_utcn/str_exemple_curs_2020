package str.curs3.ex5timer;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private JTextField timeDisplay;

    Win(Object lock) {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(200, 100);
        setLayout(null);

        timeDisplay = new JTextField();
        timeDisplay.setBounds(10, 10, 180, 20);
        timeDisplay.setEnabled(false);

        JButton startStop = new JButton("Start/Stop");
        startStop.setBounds(10, 40, 180, 20);
        startStop.addActionListener(e -> {
            synchronized (lock) {
                lock.notify();
            }
        });

        add(timeDisplay);
        add(startStop);
        setVisible(true);
    }

    public void updateTime(long delta) {
        timeDisplay.setText(delta + "");
    }
}
