package str.curs4.ex1singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        SingletonThreadUnsafe threadUnsafe1 = SingletonThreadUnsafe.getInstance();
        SingletonThreadUnsafe threadUnsafe2 = SingletonThreadUnsafe.getInstance();

        System.out.printf("threadUnsafe1 ?= threadUnsafe2 - %b", threadUnsafe1 == threadUnsafe2);

        // thread safe sigleton
        SingletonThreadSafe[] threadUnsafes = new SingletonThreadSafe[2];

        Runnable runnable1 = () -> {
            threadUnsafes[0] = SingletonThreadSafe.getInstance();
        };

        Runnable runnable2 = () -> {
            threadUnsafes[1] = SingletonThreadSafe.getInstance();
        };

        new Thread(runnable1).start();
        new Thread(runnable2).start();

        System.out.println();
        System.out.printf("threadUnsafes[0] ?= threadUnsafes[1] - %b", threadUnsafes[0] == threadUnsafes[1]);
    }
}
