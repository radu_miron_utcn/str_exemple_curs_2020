package str.curs4.ex1singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class SingletonThreadSafe {
    private static volatile SingletonThreadSafe instance;
    private String fileName;

    private SingletonThreadSafe() {
    }

    public static SingletonThreadSafe getInstance() {
        synchronized (SingletonThreadSafe.class) {
            if (instance == null) {
                instance = new SingletonThreadSafe();
            }
        }

        return instance;
    }

    public void writeLineInFile() {
        // todo implement
    }

    public String readFile() {
        // todo implement
        return null;
    }
}
