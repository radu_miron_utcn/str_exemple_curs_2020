package str.curs4.ex1singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class SingletonThreadUnsafe {
    private static SingletonThreadUnsafe instance;
    private String fileName;

    private SingletonThreadUnsafe() {
    }

    public static SingletonThreadUnsafe getInstance() {
        if (instance == null) {
            instance = new SingletonThreadUnsafe();
        }

        return instance;
    }

    public void writeLineInFile() {
        // todo implement
    }

    public String readFile() {
        // todo implement
        return null;
    }
}
