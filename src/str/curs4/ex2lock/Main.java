package str.curs4.ex2lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        new MyThreadLock(lock).start();
        new MyThreadLock(lock).start();
    }
}
