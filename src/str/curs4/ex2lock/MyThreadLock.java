package str.curs4.ex2lock;

import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThreadLock extends Thread {
    private Lock lock;

    public MyThreadLock(Lock lock) {
        this.lock = lock;
    }

    public void run() {
        this.activity(1);

        lock.lock();
        try {
            this.activity(2);
        } finally {
            lock.unlock();
        }

        this.activity(3);
    }

    private void activity(int n) {
        System.out.println(Thread.currentThread().getName() + " - is executing activity " + n);

        try {
            Thread.sleep(n == 2 ? 5000 : 1000);
        } catch (InterruptedException e) {
        }
    }
}