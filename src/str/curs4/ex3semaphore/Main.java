package str.curs4.ex3semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1);
        new MyThreadSemaphore(semaphore).start();
        new MyThreadSemaphore(semaphore).start();
    }
}
