package str.curs4.ex3semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThreadSemaphore extends Thread {
    private Semaphore semaphore;

    public MyThreadSemaphore(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public void run() {
        this.activity(1);

        try {
            semaphore.acquire();
            this.activity(2);
        } catch (InterruptedException ignored) {
        } finally {
            semaphore.release();
        }

        this.activity(3);
    }

    private void activity(int n) {
        System.out.println(Thread.currentThread().getName() + " - is executing activity " + n);

        try {
            Thread.sleep(n == 2 ? 5000 : 1000);
        } catch (InterruptedException e) {
        }
    }
}