package str.curs4.ex4cyclicbarrier;

import java.util.concurrent.CyclicBarrier;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Main {
    public static void main(String[] args) {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3,
                new CBRunnable());

        new ThreadCB(cyclicBarrier).start();
        new ThreadCB(cyclicBarrier).start();
        new ThreadCB(cyclicBarrier).start();
    }

    private static class CBRunnable implements Runnable {
        private static int iterationNum = 0;

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + ": iteration " + iterationNum++);
        }
    }
}
