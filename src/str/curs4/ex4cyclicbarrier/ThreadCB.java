package str.curs4.ex4cyclicbarrier;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadCB extends Thread {
    private CyclicBarrier cyclicBarrier;

    public ThreadCB(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        while (true) { // it works every time
            activityBeforeSynch();

            try {
                cyclicBarrier.await();
            } catch (InterruptedException ignored) {
            } catch (BrokenBarrierException ignored1) {
            }

            activityAfterSynch();
        }
    }

    private void activityAfterSynch() {
        System.out.println(Thread.currentThread().getName() + ": simulate activity after the barrier");
    }

    private void activityBeforeSynch() {
        System.out.println(Thread.currentThread().getName() + ": simulate activity before the barrier");
        try {
            Thread.sleep((new Random().nextInt(4) + 2) * 1000);
        } catch (InterruptedException ignored) {
        }
        System.out.println(Thread.currentThread().getName() + ": done");
    }
}
