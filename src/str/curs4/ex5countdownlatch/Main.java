package str.curs4.ex5countdownlatch;

import str.curs4.ex4cyclicbarrier.ThreadCB;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Main {
    public static void main(String[] args) {

        CountDownLatch countDownLatch = new CountDownLatch(3);

        new ThreadCDL(countDownLatch).start();
        new ThreadCDL(countDownLatch).start();
        new ThreadCDL(countDownLatch).start();
    }
}
