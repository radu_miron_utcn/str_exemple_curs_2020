package str.curs4.ex5countdownlatch;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadCDL extends Thread {
    private CountDownLatch countDownLatch;

    public ThreadCDL(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
//        while (true) { // it works only once; cannot use CDL in a loop
        activityBeforeSynch();

        countDownLatch.countDown();

        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {
        }

        activityAfterSynch();
//        }
    }

    private void activityAfterSynch() {
        System.out.println(Thread.currentThread().getName() + ": simulate activity after the barrier");
    }

    private void activityBeforeSynch() {
        System.out.println(Thread.currentThread().getName() + ": simulate activity before the barrier");
        try {
            Thread.sleep((new Random().nextInt(4) + 2) * 1000);
        } catch (InterruptedException ignored) {
        }
        System.out.println(Thread.currentThread().getName() + ": done");
    }
}
