package str.curs4.ex6waitnotifywithsemaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class WaiterThread extends Thread {
    public void run() {
        try {
            Main.semaphore.acquire();
        } catch (InterruptedException ignored) {
        }
        System.out.println("I was notified");

    }
}
