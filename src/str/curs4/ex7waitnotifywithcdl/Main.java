package str.curs4.ex7waitnotifywithcdl;

import str.curs4.ex6waitnotifywithsemaphore.NotifierThread;
import str.curs4.ex6waitnotifywithsemaphore.WaiterThread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
*/

public class Main {
    static final CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) {
        new NotifierThread().start();
        new WaiterThread().start();
    }
}

