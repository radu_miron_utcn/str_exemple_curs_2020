package str.curs4.ex7waitnotifywithcdl;


/**
 * @author Radu Miron
 * @version 1
 */

public class NotifierThread extends Thread {
    public void run() {
        System.out.println("Lets make the waiter wait");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }

        Main.countDownLatch.countDown();
    }
}