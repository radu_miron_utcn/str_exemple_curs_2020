package str.curs4.ex7waitnotifywithcdl;

/**
 * @author Radu Miron
 * @version 1
 */
public class WaiterThread extends Thread {
    public void run() {
        try {
            Main.countDownLatch.await();
        } catch (InterruptedException ignored) {
        }
        System.out.println("I was notified");

    }
}
